emails = [
    'gaurav@consultadd.com',
    'anikesh@consultadd.com',
    'dimple@consultadd.com',
    'suhas@consultadd.com',
    'ayushi@consultadd.com'
]

fetchNavigate = () => {
    fetch('https://demo7857661.mockable.io/testdata')
        .then(response => response.json())
        .then(data => {
            // console.log(data)
            localStorage.setItem('user_name', data['firstName'])
            sessionStorage.setItem('age', data['age'])
            window.location.href = 'home.html'
        })
        .catch(err => console.log(err))
}



$(document).ready(function () {

    console.log(1)
    var form = $("<form>")
    form.attr('method', "post")
    form.attr('action', 'home.html')
    form.attr('id', 'myform')

    var span = $("<span>")
    span.attr('id', 'status')

    var div = $("<div>")
    div.addClass("container")

    var user = $("<input>")
    user.attr('type', "text")
    user.attr('name', "username")
    user.attr('id', 'name')
    user.attr('placeholder', 'Email ID')
    // user.placeholder = 'Email ID'
    user.bind('keyup', () => {
        form_ = document.getElementById('myform')
        email = form_.elements[0].value
        if (email.length == 0) {
            span.innerText = ''
            return
        }
        bool = validate(email)
        if (bool) {
            $('#status').text('valid')
            $('#status').css('color', 'green')
        }
        else {
            $('#status').text('invalid')
            $('#status').css('color', 'red')
        }
    })

    var pass = $("<input>")
    pass.attr('type', "password")
    pass.attr('id', 'password')
    pass.attr('name', "password")
    pass.attr('placeholder', 'Password')
    // pass.placeholder = 'Password'

    var s = $("<input>")
    s.attr('type', "submit")
    s.attr('value', "Submit")
    s.addClass("btn")
    s.bind("click", function (event) {

        form_ = $('#myform')
        console.log(form_)

        email = $('#name').val()
        password = $('#password').val()
        console.log(email)
        bool = validate(email)
        if (bool) {
            M.toast({ html: 'Valid User ;)' })
            event.preventDefault()
            fetchNavigate()
        }
        else {
            M.toast({ html: 'Invalid User :(' })
            event.preventDefault()
        }
    });

    div.append(form)
    form.append(user)
    form.append(span)
    form.append(pass)
    form.append(s)
    $('body').append(div)


    validate = (email) => {
        var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (pattern.test(email)) {
            for (email_ of emails) {
                if (email == email_) {
                    return true
                }
            }
        }
        else {
            return false
        }
    }
});